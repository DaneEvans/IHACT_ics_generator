# -*- coding: utf-8 -*-
"""
Created on Wed Oct 25 15:28:56 2017

@author: dane.evans
"""

#%%

# take the IHACT excel file, change the formats of the date column tio custom > yyyymmdd
#                                                  and time columns to custom > hhmmss
# check that the league you want is selected, and that the data in that column is complete and consistent (it's case sensitive)
# save as a .csv file. 

#  run this program, and upload. recommend doing this on a machine that is in 
#        the correct timezone as I haven't accounted for that 







import os
import time
import csv
import tkinter
from tkinter import *
from tkinter import filedialog
import random


CalVersion = '0' 

def generate_header(filepath):
    file_path_ext = '{}.ics'.format(filepath)
    with open(file_path_ext, 'w') as hf:
        hf.write('BEGIN:VCALENDAR\n')
        hf.write('PRODID: IHACT BS calculator\n')
        hf.write('VERSION:2.0\n\n')
        hf.write('X-WR-TIMEZONE:Australia/Sydney\n')
    return file_path_ext

def generate_footer(file_path_ext):
    with open(file_path_ext, 'a') as hf: ## append mode
            hf.write('\n\nEND:VCALENDAR\n\n\n')
            hf.write('//////////////////////////////////////////////////\n')
            hf.write('Auto-Generated File by IHACT_converter.py \n')
            hf.write('//////////////////////////////////////////////////\n')
        
       
def generate_event(file_path_ext,details):
    with open(file_path_ext, 'a') as hf: ## append mode
        hf.write('\n\nBEGIN:VEVENT\n')
        hf.write('SUMMARY:{}\n'.format(details[0]))
        hf.write('UID: {}@IHACT{}{}\n'.format(time.time(),CalVersion,random.random()))
        hf.write('DTSTART:{}\n'.format(details[1]))
        hf.write('DTEND:{}\n'.format(details[2]))    
#        hf.write('SEQUENCE:1\n')
#        hf.write('STATUS:CANCELLED\n')
        hf.write('END:VEVENT\n')
        
def generate_neg_event(file_path_ext,details):
    with open(file_path_ext, 'a') as hf: ## append mode
        hf.write('\n\nBEGIN:VEVENT\n')
        hf.write('SUMMARY:{}\n'.format(details[0]))
        hf.write('UID: {}@IHACT{}\n'.format(time.time(),CalVersion,random.random()))
        hf.write('DTSTART:{}\n'.format(details[1]))
        hf.write('DTEND:{}\n'.format(details[2]))    
        hf.write('SEQUENCE:1\n')
        hf.write('STATUS:CANCELLED\n')
        hf.write('END:VEVENT\n')
        
def opencsv2list(file):
    with open(file, 'r') as csvfile:
        reader = csv.reader(csvfile)
        slotlist = list(reader)
        return slotlist

def generate_ics(ListMask, LeagueList,outFile, data):       
    file = generate_header(outFile)

    for i in range(len(ListMask)):
        if ListMask[i]:
            generate_cal_entries(LeagueList[i],data,file)
        
    generate_footer(file) 
    
    print(file)
            
def create_outFile (input_file, outName):
    input_fileDir = input_file[0:input_file.rfind('/')+1]        
    outFile = str(input_fileDir) + str(outName)
    return outFile

    
def generate_cal_entries(league, listrows, file):
    for i in range(len(listrows)):
        if (listrows[i][1] == league):  # compare leagues
            if (listrows[i][6] == 'Game'):
                title = str(listrows[i][6])+':'+str(listrows[i][7])+ ' vs '+  str(listrows[i][8]) # games
            else:
                title = str(listrows[i][6] ) # practice
            dateStr = str(listrows[i][3])    
            startTime = str(listrows[i][4])
            endTime = str(listrows[i][5])
            generate_event(file, [title, dateStr+'T'+startTime ,dateStr+'T'+endTime] )
        


def main(league):
    csvfile = 'D:\Dane\Downloads\dos.csv' # pre sanitised to the correct output formats. 
    
    listrows = opencsv2list(csvfile) 
    #[[row0]....]
    #row0[Org,league,day, date, startT, endT,Home Team, Away Team, Notes, [rink details x 3 ]]
    file = generate_header(league)
    

    generate_cal_entries(league, listrows, file )


    generate_footer(file) 


def get_league_options(data):
    leagueoptions = []
    for i in range(len(data)-1):
        option = str(data[i+1][1])
        try: 
            leagueoptions.index(option)
        except ValueError:
            leagueoptions.append(option)
        else:
            "do nothing"
            
    leagueoptions.remove('')        
    leagueoptions.sort()        
            
    return leagueoptions
   
    




   ### GUI Implementation    

def gui_intro():
    """make the GUI version of this command that is run if no options are
    provided on the command line"""
    def button_next_callback():
        """ What to do when the Browse button is pressed """
        root.destroy()
        return 0 

    root = tkinter.Tk()
    frame = tkinter.Frame(root,width = 360, height = 1 )
    frame.pack()

    statusText = tkinter.StringVar(root)
    statusText.set("Press the Next button to continue")
    

    label = tkinter.Label(root, text=
                          " Please prepare the .csv file as follows: \n"
                             " - opent the IHACH excel file \n"
                             " - change date columns to format > custom YYYYMMDD \n"
                             " - change time columns to format > custom HHMMSS \n"
                             " - Save As > comma separated variable (.csv)\n\n\n"
                             "                                Version 1.1 \n"
                             "                            Dane Evans 2017 \n", 
                             justify=LEFT)
    label.pack()
    
    button_next = tkinter.Button(root,
                       text="Next",
                       command=button_next_callback)
    button_next.pack()

    message = tkinter.Label(root, textvariable=statusText)
    message.pack()

    tkinter.mainloop()            
    
def gui_pg1():
    """make the GUI version of this command that is run if no options are
    provided on the command line"""
    def button_browse_callback():
        """ What to do when the Browse button is pressed """
        filename = filedialog.askopenfilename()
        entry.delete(0, END)
        entry.insert(0, filename)
    def button_next_callback():
        """ What to do when the Browse button is pressed """
        input_file = entry.get()
        if input_file.rsplit(".")[-1] != "csv":
            statusText.set("Filename must end in `.csv'")
            message.configure(fg="red")
            return 1
        else:          
            listrows = opencsv2list(input_file) 
            if listrows is None:
                statusText.set("Error reading file `{}'".format(input_file))
                message.configure(fg="red")
                return 1 
            
            root.destroy()
            gui_last(listrows,input_file)
            

    root = tkinter.Tk()
    frame = tkinter.Frame(root,width = 360, height = 1 )
    frame.grid()

    statusText = tkinter.StringVar(root)
    statusText.set("Enter CSV filename, "
                   "then press the Next button")

    label = tkinter.Label(root, text="CSV file: ")
    label.grid()
    
    button_browse = Button(root,
                       text="Browse",
                       command=button_browse_callback)
    button_browse.grid(column = 1, row = 2)
    
    entry = Entry(root, width=50)
    entry.grid(column = 0, row = 2)
    
    button_go = tkinter.Button(root,
                       text="Next",
                       command=button_next_callback)
    button_go.grid()

    message = tkinter.Label(root, textvariable=statusText)
    message.grid()

    tkinter.mainloop()
    

def gui_last(dataArray,input_file): # takes the array of data   
    LeagueList = get_league_options(dataArray)
    LeagueNum = len(LeagueList)
                
    root = Tk()
    frame = tkinter.Frame(root,width = 360, height = 1 )
    frame.grid()
    
    statusText = tkinter.StringVar(root)
    statusText.set("Select Leagues to Generate Cal for, "
                   "then press the Go button")

    label = tkinter.Label(root, text="Leagues: ")
    label.grid()
    
    def button_go_callback():
        Sel_Leagues = []
        for i in range(LeagueNum):
            Sel_Leagues.append(C[i].get())
        if sum(Sel_Leagues) == 0:
            statusText.set("No Leagues selected")
            message.configure(fg="red")
            return
        else:
            outName = outputName.get()
            outFile = create_outFile (input_file, outName)
            generate_ics(Sel_Leagues,LeagueList, outFile, dataArray)
            
            root.destroy()    
            gui_extro(outFile)
        
            
    C = [0]*LeagueNum
    
    for league in range(LeagueNum):
        #LeagueList[league] = Variable()
        C[league] = BooleanVar()
        l = Checkbutton(root, text = LeagueList[league], variable=C[league])
        #LeagueList[league] = Variable()
        #l = Checkbutton(root, text = LeagueList[league], variable=LeagueList[league])
        l.grid(sticky = 'W')


    label = tkinter.Label(root, text="Output File will be placed in the input Directory.\n"
                          "File Name: ")
    label.grid()
    
    v = StringVar()
    outputName = Entry(root, width=50, textvariable =v )
    v.set("NewCal")
    outputName.grid()
    

    button_go = tkinter.Button(root,
                       text="Go",
                       command=button_go_callback)
    button_go.grid()
    



    message = Label(root, textvariable=statusText)
    message.grid()



    root.mainloop()
    
def gui_extro(outDir):
    """make the GUI version of this command that is run if no options are
    provided on the command line"""
    def button_next_callback():
        """ What to do when the Browse button is pressed """
        root.destroy()

    root = tkinter.Tk()
    frame = tkinter.Frame(root,width = 360, height = 1 )
    frame.pack()

    statusText = tkinter.StringVar(root)
    statusText.set("Press the Close button to exit")
    

    label = tkinter.Label(root, text=
                          " Your Output file can be found at:\n {}.ics\n".format(outDir)
                             , 
                             justify=LEFT)
    label.pack()
    
    button_next = tkinter.Button(root,
                       text="Close",
                       command=button_next_callback)
    button_next.pack()

    message = tkinter.Label(root, textvariable=statusText)
    message.pack()

    tkinter.mainloop()      


    
if __name__ == "__main__":
   """ Run as a stand alone script 
    
   """
   
   gui_intro()
   
   gui_pg1()

    # main('Womens') # -- non GUI version